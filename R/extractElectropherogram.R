#' Extract electropherogram matrix from ABIF object.
#'
#' Extract the electropherogram as a 4-column matrix from an ABIF object
#' (from read.abif() in Bioconductor package sangerseqR). Also normalizes
#' the electropherogram such that the mean amplitude over a given range is
#' one.
#' @param abif The abif object obtained from read.abif().
#' @param normLim A two-number vector denoting the range over which the signal
#' should be normalized. If set to NULL, no normalization is performed.
#' @examples
#' # load an example abif object from a .ab1 file from a Ab37xx sequencer
#' data('exampleAbifObject')
#' # get the relevant fluorescence data in a matrix format
#' mat = extractElectropherogram(exampleAbifObject, c(1500, 9000))
#' # plot the fluorescence traces
#' matplot(mat, type='l', xlim=c(2000,3000), lty=1)
#' # verify that it's normalized so that the mean is one
#' mean(mat[1500:9000,])
#' @export
extractElectropherogram = function(abif,  normLim=NULL){
  dat = cbind(abif@data$DATA.9, abif@data$DATA.10, abif@data$DATA.11, abif@data$DATA.12)
  if (!is.null(normLim)) # Normalize fluorescence readings
    dat = dat/mean(dat[normLim[1]:normLim[2],])
  return(dat)
}
