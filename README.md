![CASEU logo](images/caseuLogo.png)

### What is this repository for? ###

* CASEU is an R package to determine community composition from a mixed electropherogram. 
* CASEU stands for Community Analysis via Sanger Electropherogram Unmixing.
* We pronounce it 'cashew'.

### How do I get set up? ###

Take a look at the [package vignette](https://htmlpreview.github.io/?https://bitbucket.org/dattamanoshi/caseu/raw/master/doc/CASEU_Vignette.html).

If you're in a huge hurry to get up and running, open R and try the following:

```r
install.packages('remotes')
remotes::install_bitbucket('dattamanoshi/caseu')
library(CASEU)

## You can run analyses using a graphical interface
fitSangerMixtureGUI()

## Or you can run it at the console
data('fourStrainExpt')              # load an example dataset
results = fitSangerMixture(mixture=fourStrainExpt$mixture,
                           components = fourStrainExpt$components,
                           verbose=TRUE)
print(results)                      # view the results
plot(results)                       # plot the fit
sangerFitDiagnosticPlot(results)    # plot a bunch more diagnostics
```

### Who do I talk to? ###

Manoshi Datta <mdatta8788@gmail.com> or Nate Cermak <cerman07@protonmail.com>
