---
title: "Getting Started with CASEU"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{CASEU_Vignette}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
options(max.print=100)
```

## What is CASEU?

CASEU (pronounced "cashew") is an R package to determine community composition from a mixed electropherogram. CASEU stands for Community Analysis via Sanger Electropherogram Unmixing. 

CASEU is useful for measuring the fractional abundances of individual amplicons in a mixture of amplicons. Experimentally, it requires Sanger sequencing of both the mixture and the pure amplicons. 

![CASEU workflow illustration](../inst/extdata/caseuWorkflow.png)

## Example workflow

For example, suppose you have four strains of bacteria: A, B, C, and D. You mix all four at equal concentrations in a test tube, grow the bacteria for some time, and now want to know what the proportions are of each bacterial strain. Perhaps strains A and B significantly outcompete strains C and D, so the mixture is now 50% A and 50% B. Perhaps strain C takes over and is now 100% of the population. How do you measure the community composition? This is the question CASEU addresses.

To use CASEU to answer this question, you would first extract DNA from the mixture and PCR amplify a marker gene that is present in all four strains. Importantly the marker gene should have identical primer regions in all three strains, but differ in the intervening sequence. We have tested CASEU using the 16S ribosomal gene and standard 27F/1492R primers, but other genes can potentially be used as well. You would then Sanger sequence the resulting amplicon mixture, as well as pure 16S amplicon samples from strains A, B, C, and D. Thus, this experiment would require Sanger sequencing of 5 samples in total.

Assuming you don't operate the Sanger sequencer yourself, you most likely submitted your samples either to a university core facility or a company such as Genewiz. Regardless, you should receive back, among other things, the raw data files from the sequencer. These files should have a ".ab1" file extension (for Applied Biosystems; the maker of many Sanger sequencers). CASEU requires these .ab1 files, and cannot operate on simple sequence files like fasta files. 

Note also that when sequencing mixtures of amplicons, companies like Genewiz will likely report that sequencing the mixture failed  in some way, and will have a very low quality score. That's expected, since the sequencer and facility are expecting samples containing only a single sequence, and of course yours might have multiple sequences (depending on whether one strain took over or not). 

### Getting started with R and RStudio

If you haven't already, install R and RStudio. CASEU is an R package and thus does not work on its own. On the plus side, like R and RStudio, it is cross-platform and should work fine on Windows, Mac OS, and Linux (tested on Ubuntu 18.04).

Open RStudio, create a new R script, and try out the commands in the order given below.

### Loading required libraries

To get started using CASEU, you'll need to first download and install CASEU. Note that you only need to do this part once, so if you add it to your script, you can comment it out after you've run it once:

```{r installPackages, message=FALSE, eval=FALSE}
# download and install remotes package, which includes the install_bitbucket() function
install.packages('remotes')

# download and install caseu from the bitbucket repository
remotes::install_bitbucket('dattamanoshi/caseu')

# download and install sangerseqR package
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install("sangerseqR")
```

Now you need to load CASEU and one other necessary package called sangerseqR. Packages are not loaded by default when you start R - if you want specific functions like those in the CASEU package, you need to load the package. 

```{r setup, message=FALSE}
# load CASEU, which contains fitting functions to estimate fractional composition
library(CASEU)

# load sangerseqR, which has the function for reading .ab1 files - read.abif()
library(sangerseqR)
```


### Loading data

We've included some example files that mirror the exact use case described above. We mixed amplicons for four strains at known ratios, and used CASEU to measure these known ratios). You can use the function system.file() to find the path for these files on your computer (they're stored wherever R installed CASEU), like so:


```{r getFileLocations, message=FALSE}
strainA_Ab1File = system.file('extdata','NC-01-27F.ab1', package='CASEU')
strainB_Ab1File = system.file('extdata','NC-13-27F.ab1', package='CASEU')
strainC_Ab1File = system.file('extdata','NC-19-27F.ab1', package='CASEU')
strainD_Ab1File = system.file('extdata','NC-31-27F.ab1', package='CASEU')

mixture1File =    system.file('extdata','NC-38-27F.ab1', package='CASEU')
```

Now we have the paths for four single-strain Sanger sequencing files, and a mixture file. We load these files as "abif" objects using read.abif() in R.


```{r loadData, message=FALSE}
strainA_Abif = read.abif(strainA_Ab1File)
strainB_Abif = read.abif(strainB_Ab1File)
strainC_Abif = read.abif(strainC_Ab1File)
strainD_Abif = read.abif(strainD_Ab1File)

mixture1_Abif = read.abif(mixture1File)
```

We can take a quick look at what an abif object consists of, using the function str(). 

```{r peekAtAbif}
str(strainA_Abif, list.len=5)
```

abif objects clearly contain a ton of things, for which a bare minimum of documentation can be found online: https://projects.nfstc.org/workshops/resources/articles/ABIF_File_Format.pdf. However, the parts we care about are the four fluorescence signals for each channel. These are stored in a sub-object called "data" (accessed using the @ symbol), which has associated vectors DATA.9, DATA.10, DATA.11, and DATA.12. These vectors can be accessed like so:

```{r peekAtAbif2}
print( strainA_Abif@data$DATA.9 )
```

CASEU includes a simple function called extractElectropherogram() that takes an abif object, extracts these vectors, combines them into a matrix, and normalizes them (scaling such that the mean fluorescence over a given region is 1).

```{r extractElectropherograms}
sA = extractElectropherogram(strainA_Abif, normLim = c(1500,9000))
sB = extractElectropherogram(strainB_Abif, normLim = c(1500,9000))
sC = extractElectropherogram(strainC_Abif, normLim = c(1500,9000))
sD = extractElectropherogram(strainD_Abif, normLim = c(1500,9000))

m1 = extractElectropherogram(mixture1_Abif, normLim = c(1500,9000))
```

These variables are now just four-column matrices, in which each column represents a fluorescent channel, and each row is a timepoint in the electropherogram. 

```{r showExtractElectropherogramResults}
str(sA)
head(sA)
```

### Exploring the raw data

We can plot one of these matrices using matplot() to take a look at it. matplot() plots each column as a line of a different color.  

```{r, plotElectropherogram, fig.width=8, fig.height=4}
matplot(sA, type='l', lty=1, xlab='time (indices)')
```

If we zoom in a short stretch of the x-axis (say from indices 1500-2000), it looks a bit more like what we expect from an electropherogram.

```{r, plotElectropherogram2, fig.width=8, fig.height=4}
matplot(sA, type='l', lty=1, xlab='time (indices)', xlim=c(1500,2000))
```

Note that that was the electropherogram for a single strain. A mixed electropherogram looks quite a bit different, because it contains components from multiple different signals. Let's take a look at the first mixture. 

```{r plotElectropherogram3, fig.width=8, fig.height=4}
matplot(m1, type='l', lty=1, xlab='time (indices)', xlim=c(1500,2000))
```


### Measuring fractions by fitting a mixture

How do we estimate the fractions of individual components in m1? We run fitSangerMixture(), which is the workhorse function in CASEU. Note that this line might take a minute or two to run.

```{r fitMixture, cache=TRUE}
result1 = fitSangerMixture(mixture = m1, components = list(A=sA, B=sB, C=sC, D=sD), verbose=TRUE)
```

When we use "verbose=TRUE", we get this output that tells us how each iteration of fitting goes. After 4 iterations, all strains have been aligned. How do we get the fractions? We can just print the "result1" object, like so:

```{r printresult1}
print(result1)
```

This shows the fraction of strain, as well as the alignment parameters for each strain. There are 6 alignment parameters per strain, and their deviation from the "knot positions"" noted towards the top (1500, 3000, etc.) is how a much each segment boundary was shifted.


### Diagnostics (do we believe the results?)

It's important to know whether we should believe these results. We can first note the fit $R^2$, which is given as 0.93 in the summary above (also accessible via "result1$r2"). This says that we can account for 93% of the variance in the mixture electropherogram as coming from the four strains. Typically we expect to see $R^2$ above 0.90 - below that can indicate a problem.

We can also manually inspect the fit by plotting the result. Below, we plot first the full fit, and then zoom into a section near the beginning.

```{r plotfit, fig.width=8, fig.height=8}
par(mfrow=c(2,1), mar=c(4,4,1,1))
plot(result1)
plot(result1, xlim=c(2000,2500))
```

Overall, the red lines look like they're pretty similar to the black lines, suggesting that our mixture is well-fit by a linear combination of our (aligned) individual strains. 

We can also look at a handful of related diagnostic plots by calling sangerFitDiagnosticPlot():

```{r sangerFitDiagnosticPlot1, fig.width=8, fig.height=8}
sangerFitDiagnosticPlot(result1)
```
In the upper left corner, we can see the residuals for each channel over time. From this plot, we can see that one channel has a typically negative residual, whereas the other channels tend to have positive residuals that decay in amplitude over time. The dashed grey lines indicate zero for each channel (channels are vertically offset for clarity). In general, the fit residuals get smaller over time (which is also shown in the lower left plot). The red line is a moving average filter applied to the raw residuals (black lines). 

The upper right plot shows the alignment parameters for each strain, and in particular, how much each boundary point was moved in the warping. We can see that strain 1 required the most significant absolute shifting. A non-zero slope in lines on this plot means that segments were contracted or stretched.  

### Comparing multiple samples 
Now, let's suppose we have two other mixture files we want to compare to. Perhaps these also contained strains A, B, C, and D, but the growth conditions were different. Let's fit them as well, and then compare them.

```{r dostuff, fig.width=5,fig.height=8}

# get the locations for two more mixture files
mixture2File =    system.file('extdata','NC-40-27F.ab1', package='CASEU')
mixture3File =    system.file('extdata','NC-42-27F.ab1', package='CASEU')

# load the data as "abif" objects
mixture2_Abif = read.abif(mixture2File)
mixture3_Abif = read.abif(mixture3File)

# convert them into matrices and normalize them
m2 = extractElectropherogram(mixture2_Abif, normLim = c(1500,9000))
m3 = extractElectropherogram(mixture3_Abif, normLim = c(1500,9000))

# fit mixtures
result2 = fitSangerMixture(mixture = m2, components = list(A=sA, B=sB, C=sC, D=sD))
result3 = fitSangerMixture(mixture = m3, components = list(A=sA, B=sB, C=sC, D=sD))

# compare all three mixtures
par(mfrow=c(3,1))
barplot(result1$frac, main = "mixture 1")
barplot(result2$frac, main = "mixture 2")
barplot(result3$frac, main = "mixture 3")

```





