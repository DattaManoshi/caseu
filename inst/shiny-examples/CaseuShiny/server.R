

library(shiny)
library(CASEU)
library(parallel)

if (!require(sangerseqR)){
  source("https://bioconductor.org/biocLite.R")
  biocLite("sangerseqR")
  library(sangerseqR)
}


"%+%" <- function(x,y) paste(x,y,sep='')

function(input, output) {
  knots = reactive({seq(input$startIndex,input$endIndex,length.out=input$numSegments+1)})
  shiftPenalty = reactive({input$shiftPenalty})
  tol = reactive({input$tol})
  ranges <- reactiveValues(x = NULL, y = NULL)
  shiftGrid = reactive({ seq(-input$maxShift, input$maxShift, by=2) })
  stretchGrid = reactive({ seq(-input$maxStretch, input$maxStretch, by=2) })

  # load in reference data
  referenceData = reactive({
    if (is.null(input$referenceFiles))
      return(list())
    else {
      components = lapply(input$referenceFiles$datapath,
                         function(i) extractElectropherogram(read.abif(i), range(knots())))
      names(components) = input$referenceFiles$name
      return(components)
    }
  })

  mixtureData = reactive({
    if (is.null(input$mixtureFiles))
      return(list())
    else {
      return(lapply(input$mixtureFiles$datapath,
                    function(i) extractElectropherogram(read.abif(i), range(knots()))))      }
  })


  output$refDataLoaded <- reactive({   length(referenceData()) > 1   })
  output$allDataLoaded <- reactive({   length(referenceData()) > 0 & length(mixtureData()) > 0  })

  # tables should show names of files that will be/have been analyzed
  output$referenceFileTable <- renderTable({
    data.frame(ReferenceFile=input$referenceFiles$name)
  })
  output$mixtureFileTable <- renderTable({
    data.frame(MixtureFiles=input$mixtureFiles$name)
  })


  # if user clicks "Run Analysis", then run runAnalysis()
  observeEvent(input$runAnalysis, {runAnalysis()})

  observeEvent(input$allRefPairs, {allRefPairs()})

  runAnalysis = function() {
    withProgress(message = 'Fitting in progress...', value = 0.0, {
      fits = list()
      i=1
      cl = makeCluster(detectCores()-1)
      for (mixture in mixtureData()){
        setProgress((i-1)/(length(mixtureData())))
        fits[[i]] = fitSangerMixture(mixture,
                    components=referenceData(),
                    shiftGrid=shiftGrid(),
                    stretchGrid=stretchGrid(),
                    knots=knots(), shiftPenalty=shiftPenalty(),
                    tol=tol(), cl=cl, verbose=TRUE)
        i=i+1
      }
      stopCluster(cl)


      output$fitPlot <- renderPlot({
        par(mfrow=c(length(fits),1), mar=c(3,3,1,1))
        for (i in 1:length(fits)){
          plot(fits[[i]], main=input$mixtureFiles$name[i], xlim=ranges$x, ylim=ranges$y)
        }
      })

      df = data.frame(mixtureFiles = input$mixtureFiles$name,
                      frac = t(sapply(fits, function(x) x$frac)),
                      R2 = as.numeric(sapply(fits, function(x) x$r2)))
                      #alignmentParams = t(sapply(fits, function(x) x$alignmentParams)))

      output$resultTable = renderTable(df, digits=c(2, rep(2,length(referenceData()))))
    })
  }

  allRefPairs = function(){
    withProgress(message = 'Fitting in progress...', value = 0.0, {
      fits = list()
      i=1
      cl = makeCluster(detectCores()-1)
      result = alignedCorrelationMatrix(referenceData(),
                                     shiftGrid=shiftGrid(),
                                     stretchGrid=stretchGrid(),
                                     knots=knots(), shiftPenalty=shiftPenalty(),
                                     tol=tol(), cl=cl, verbose=TRUE)
      stopCluster(cl)
      output$fitPlot <- renderPlot({
        image(result$corMat, zlim=c(0,1), col=gray(0:100/100), axes=FALSE)
        axis(1, at=seq(0,1, length.out=nrow(result$corMat)), labels=rownames(result$corMat), las=2)
        axis(2, at=seq(0,1, length.out=nrow(result$corMat)), labels=rownames(result$corMat), las=2)
      })
      output$resultTable = renderTable(result$corMat, digits=rep(3,length(referenceData())), rownames=TRUE)
    })
  }

  # Next 12 lines taken from https://gallery.shinyapps.io/105-plot-interaction-zoom/
  # When a double-click happens, check if there's a brush on the plot.
  # If so, zoom to the brush bounds; if not, reset the zoom.
  observeEvent(input$fitPlot_dblclick, {
    brush <- input$fitPlot_brush
    if (!is.null(brush)) {
      ranges$x <- c(brush$xmin, brush$xmax)
      ranges$y <- c(brush$ymin, brush$ymax)
    } else {
      ranges$x <- NULL
      ranges$y <- NULL
    }
  })

  outputOptions(output, "allDataLoaded", suspendWhenHidden = FALSE)
  outputOptions(output, "refDataLoaded", suspendWhenHidden = FALSE)
}

